'use strict'

var config = require('../config');
var cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: 'dboc10l8x',
    api_key: config.cloudinaryAPIKey,
    api_secret: config.cloudinaryAPISecret
});

exports.image = async(file) => {
    await cloudinary.uploader.upload('pizza.jpg', {
        tags: 'basic_sample'
    })
    .then(function (image) {
        console.log("* " + image.public_id);
        console.log("* " + image.url);
    })
    .catch(function (err) {
        if (err) {
            console.warn(err);
        }
    });

}
