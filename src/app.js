'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors')
const conf = require('../config');

const app = express();
const router = express.Router();

mongoose.connect(conf.connectioString)

const Product = require('../models/product');
const Costumer = require('../models/costumer');
const Order = require('../models/order');
const Seller = require('../models/seller');
const Categories = require('../models/categories');

const index = require('../routes/index');
const products = require('../routes/product');
const costumer = require('../routes/costumer');
const order = require('../routes/order');
const seller = require('../routes/seller');
const category = require('../routes/category');

app.use(cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use('/', index);
app.use('/create', products);
app.use('/costumer', costumer);
app.use('/order', order);
app.use('/seller', seller);
app.use('/category', category);

module.exports = app;
