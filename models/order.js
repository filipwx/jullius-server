'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    costumer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Costumer'
    },
    cod: {
        type: String,
        required: true
    },
    createDate: {
        type: Date,
        required: true,
        default: Date.now
    },
    status: {
        type: String,
        required: true,
        enum: ['created', 'done', 'removed', 'auction'],
        default: 'created'
    },
    items: [{
        quantidade: {
            type: String,
            required: true,
            default: 1
        },
        categoria: {
            type: String,
            required: true
        },
        nome: {
            type: String,
            required: true
        }
    }],
});

module.exports = mongoose.model('Order', schema)
