'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    costumer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Costumer'
    },
    function_corp: {
        type: String
    },
    fone_corp: {
        type: String,
        required: true
    },
    name_corp: {
        type: String
    },
    createdAt: {
        type: String,
        required: true,
        default: new Date()
    },
    address: [{
        name: String,
        endereco: String,
        numero: Number,
        neiboo: String,
        state: String,
        uf: String,
        complet: String,
        defaulter: {
            type: Boolean,
            default: false
        },
        created: {
            type: Date,
            default: Date.now
        }
    }]
});

module.exports = mongoose.model('Buyer', schema)