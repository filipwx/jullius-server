'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        index: true,
        unique: true
    },
    cpf_cnpj: {
        type: Number,
        required: true,
        trim: true,
        index: true,
        unique: true
    },
    fone: {
        type: String,
        required: true
    },
    createdAt: {
        type: String,
        required: true,
        default: new Date()
    },
    type: {
        type: String,
        enum: ['buyer', 'seller']
    },
    password: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Costumer', schema)
