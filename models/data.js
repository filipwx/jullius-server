'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    costumer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Costumer'
    },
    address: {
        type: String,
        required: true
    },
    street: {
        type: String
    },
    neardby: {
        type: String
    },
    default: {
        type: Boolean,
        required: true
    },
    createdAt: {
        type: String,
        required: true,
        default: new Date()
    }
});

module.exports = mongoose.model('Data', schema)