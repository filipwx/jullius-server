'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    costumer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Costumer'
    },
    function_corp: {
        type: String,
        required: true
    },
    fone_corp: {
        type: String,
        required: true
    },
    name_corp: {
        type: String,
        required: true
    },
    createdAt: {
        type: String,
        required: true,
        default: new Date()
    },
    tags: [{
        type: String,
        required: true
    }]
});

module.exports = mongoose.model('Seller', schema)