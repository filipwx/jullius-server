'use strict'

const mongoose = require('mongoose');
const Seller = mongoose.model('Seller');

exports.getByTag = (tag) => {
  return Seller.find({
    tag: tag
  }, 'costumer name_corp tags')
}
exports.get = () => {
  return Seller.find({}, 'costumer name_corp tags');
}
exports.getById = (id) => {
  return Seller.findOne({
    id: id
  }, 'costumer name_corp tags')
}
exports.create = (data) => {
  var seller = new Seller(data);
  return seller.save()
}
exports.update = (id, data) => {
  return Seller
    .findByIdAndUpdate(id, {
      $set: {
        function_corp: data.function_corp,
        fone_corp: data.fone_corp,
        name_corp: data.name_corp,
        tags: data.tags
      }
    })
}
