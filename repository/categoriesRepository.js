'use strict'
const mongoose = require('mongoose');
const Category = mongoose.model('Categories');


exports.create = async (data) => {
  const category = new Category(data);
  await category.save()
  return category
}
exports.update = async (id, data) => {
  const category = await Category
    .findByIdAndUpdate(id, {
      $set: {
        name: data.name,
        description: data.description,
        status: data.status
      }
    });
  return category
}
exports.get = async () => {
  var res = await Category.find({});
  return res
}