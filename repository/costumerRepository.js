'use strict'
const mongoose = require('mongoose');
const Costumer = mongoose.model('Costumer');
const Seller = mongoose.model('Seller');


exports.create = async(data) => {
    const costumer = new Costumer(data);
    await costumer.save()
    return costumer
}
exports.update = async (id, data) => {
    const costumer = await Costumer
      .findByIdAndUpdate(id, {
        $set: {
          type: data.type
        }
      });
    return costumer
}
exports.authenticate = async(data) => {
    const res = await Costumer.findOne({
      email: data.email,
      password: data.password
    });
    return res;
}
exports.getProfile = async(data) => {
    const user = await Costumer.findById(data);
    const info = await Seller.findOne({costumer: data});
    return {user, info};
}
exports.getById = async (id) => {
  const res = await Costumer.findById(id);
  return res;
}
exports.getAll = async () => {
  const res = await Costumer.find({});
  return res;
}
