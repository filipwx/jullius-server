'use strict'
const mongoose = require('mongoose');
const Order = mongoose.model('Order');


exports.get = async () => {
    var res = await Order.find({});
    return res
}
exports.getSelf = (userId) => {
    return Order.find({
      costumer: userId
    });
}
exports.create = async (data) => {
    var order = new Order(data);
    await order.save()
    return order
}

exports.update = async (id, data) => {
  const order = await Order
    .findByIdAndUpdate(id, {
      $set: {
        status: data.status
      }
    });
  return order
}
