'use strict'

const express = require('express');
const router = express.Router();
const controler = require('../controlers/seller-controler')

router.get('/', controler.get);
router.get('/:tag', controler.getByTag);
router.get('/:id', controler.getById);
router.post('/', controler.post);
router.put('/:id', controler.put);

module.exports = router;
