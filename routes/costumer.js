'use strict'

const express = require('express');
const router = express.Router();
const controler = require('../controlers/costumer-controler')
const authService = require('../services/auth');

router.post('/', controler.post);
router.get('/profile', authService.authorize, controler.getProfile);
router.put('/:id', authService.authorize, controler.put);
router.post('/authenticate', controler.authenticate);

module.exports = router;
