'use strict'

const express = require('express');
const router = express.Router();
const controler = require('../controlers/order-controler')
const authService = require('../services/auth');

router.post('/', authService.authorize, controler.post);
router.put('/:id', authService.authorize, controler.put);
router.get('/', authService.authorize, controler.getSelf);

router.get('/admin', controler.get);

module.exports = router;
