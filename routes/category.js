'use strict'

const express = require('express');
const router = express.Router();
const controler = require('../controlers/categories-controler')

router.post('/', controler.post);
router.get('/', controler.get);
router.put('/:id', controler.put);

module.exports = router;
