'use strict'

const express = require('express');
const router = express.Router();
const controler = require('../controlers/products-controler')

router.get('/', controler.get);
router.get('/:slug', controler.getBySlug);
router.get('/tags/:tag', controler.getByTag);
router.post('/', controler.post);
router.put('/:id', controler.put);
router.delete('/:id', controler.delete);

module.exports = router;
