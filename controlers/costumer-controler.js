'use strict'
const cookieParser = require('cookie-parser');
const ValidationContract = require('../validator/validator');
const repository = require('../repository/costumerRepository');
const md5 = require('md5');
const conf = require('../config');
const emailService = require('../services/email');
const authService = require('../services/auth');

exports.post = async(req, res, next) => {
    let contract = new ValidationContract();

    contract.hasMinLen(req.body.name, 5, 'O Nome deve conter mais de 5 caracteres!');
    contract.isEmail(req.body.email, 'O Email Não é valido!');
    contract.hasMinLen(req.body.cpf_cnpj, 10, 'Acho que os numeros não estão corretos!');
    contract.hasMinLen(req.body.fone, 6, 'Esse numero de telefone está estranho!');
    contract.hasMinLen(req.body.password, 6, 'A Senha deve conter mais de 5 caracteres!');

    if (!contract.isValid()) {
        res.status(400).send(contract.errors()).end();
        return
    }

    try {
        // console.log(req.body);
        const costumer = await repository.create({
            name: req.body.name,
            email: req.body.email,
            cpf_cnpj: req.body.cpf_cnpj,
            fone: req.body.fone,
            password: md5(req.body.password + global.SALT_KEY)
        })
        emailService.send(req.body.email, 'Bem vindo ao Jullius', global.EMAIL_TMPL.replace('{0}', req.body.name))

        res.status(201).send({
            message: 'Usuario cadastrado com sucesso!',
            data: costumer
        });

    } catch (e) {
        res.status(400).send({
            message: 'Falha ao cadastrar'
        });
    }

};
exports.put = async (req, res, next) => {
    try {
        const costumer = await repository.update(req.params.id, req.body);
        res.status(200).send({
            message: 'Atualizado com sucesso!',
            data: costumer
        });
    } catch (e) {
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
};
exports.getProfile = async (req, res, next) => {
    try {
      const costumer = await repository.getProfile(req.headers['user-id']);
      res.status(200).send({
        message: 'Perfil encontrado',
        data: costumer
      });
    } catch (e) {
      res.status(500).send({
        message: 'Falha ao processar sua requisição'
      });
    }
};

exports.authenticate = async(req, res, next) => {
    try {
        const customer = await repository.authenticate({
          email: req.body.email,
          password: md5(req.body.password + global.SALT_KEY)
        });
        if (!customer) {
          res.status(404).send({
            message: 'Usuário ou senha inválidos'
          });
          return;
        }
        const token = await authService.generateToken({
          id: customer._id,
          email: customer.email,
          name: customer.name
        });
        res.status(201).send({
          token: token,
          data: customer
        });
    } catch (e) {
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
};

exports.refreshToken = async (req, res, next) => {
    try {
        const token = req.body.token || req.query.token || req.headers['x-access-token'];
        const data = await authService.decodeToken(token);

        const customer = await repository.getById(data.id);

        if (!customer) {
            res.status(404).send({
                message: 'Cliente não encontrado'
            });
            return;
        }

        const tokenData = await authService.generateToken({
            id: customer._id,
            email: customer.email,
            name: customer.name
        });

        res.status(201).send({
            token: tokenData,
            data: {
                email: customer.email,
                name: customer.name
            }
        });
    } catch (e) {
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
};
