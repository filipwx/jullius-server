'use strict'

const ValidationContract = require('../validator/validator');
const repository = require('../repository/sellerRepository');

exports.getByTag = (req, res, next) => {
  repository.getByTag(req.params.tag)
    .then(data => {
      res.status(200).send(data);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.getById = (req, res, next) => {
  repository.getById(req.params.tag)
    .then(data => {
      res.status(200).send(data);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.get = (req, res, next) => {
  repository.get()
    .then(x => {
      res.status(200).send({
        data: x
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};

exports.post = (req, res, next) => {
  repository.create(req.body)
    .then(x => {
      res.status(201).send({
        message: 'Perfil atualizado com sucesso!',
        retunr: x
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha'
      });
    })
};

exports.put = (req, res, next) => {
  repository.update(req.params.id, req.body)
    .then(x => {
      res.status(201).send({
        message: 'Deu certo',
        retunr: x
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha'
      });
    })
};
