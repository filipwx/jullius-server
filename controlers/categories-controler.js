'use strict'

const repository = require('../repository/categoriesRepository');

exports.post = async (req, res, next) => {
  try {
    const category = await repository.create({
      name: req.body.name,
      description: req.body.description
    })

    res.status(201).send({
      message: 'Categoria registrada com sucesso',
      data: category
    });

  } catch (e) {
    res.status(400).send({
      message: 'Falha na requisição'
    });
  }

};
exports.get = async (req, res, next) => {
  try {
    var data = await repository.get();
    res.status(200).send(data);
  } catch (error) {
    res.status(400).send(error);
  }
};
exports.put = async (req, res, next) => {
  try {
    const costumer = await repository.update(req.params.id, req.body);
    res.status(200).send({
      message: 'Atualizado com sucesso!',
      data: costumer
    });
  } catch (e) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};