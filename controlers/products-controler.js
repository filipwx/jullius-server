'use strict'

const mongoose = require('mongoose');

const Product = mongoose.model('Product');
const ValidationContract = require('../validator/validator');
const repository = require('../repository/productRepository');

exports.getByTag = (req, res, next) => {
    repository.getByTag(req.params.tag)
    .then(data => {
            res.status(200).send(data);
        })
        .catch(e => {
            res.status(400).send({
                message: 'Falha',
                error: e
            });
        })
};
exports.getBySlug = (req, res, next) => {
    repository.getBySlug(req.params.slug)
    .then(data => {
            res.status(200).send(data);
        })
        .catch(e => {
            res.status(400).send({
                message: 'Falha',
                error: e
            });
        })
};
exports.get = (req, res, next) => {
    repository.get()
    .then(x => {
            res.status(200).send({
                data: x
            });
        })
        .catch(e => {
            res.status(400).send({
                message: 'Falha',
                error: e
            });
        })
};

exports.post = (req, res, next) => {
    let contract = new ValidationContract();

    contract.hasMinLen(req.body.title, 5, 'O titulo deve conter mais de 5 caracteres!');
    contract.hasMinLen(req.body.slug, 5, 'O slug deve conter mais de 5 caracteres!');
    contract.hasMinLen(req.body.description, 5, 'A descrição deve conter mais de 5 caracteres!');
    contract.hasMinLen(req.body.price, 2, 'O preco deve conter mais de 2 caracteres!');

    if (!contract.isValid()) {
        res.status(400).send(contract.errors()).end();
        return
    }

    repository.create(req.body)
    .then( x => {
        res.status(201).send({
            message: 'Deu certo',
            retunr: x._id
        });
    })
    .catch( e => {
        res.status(400).send({message: 'Falha'});
    })

};

exports.put = (req, res, next) => {
    repository.update(req.params.id, req.body)
    .then(x => {
        res.status(201).send({
            message: 'Deu certo',
            retunr: x
        });
    })
    .catch(e => {
        res.status(400).send({
            message: 'Falha'
        });
    })
};

exports.delete = (req, res, next) => {
    Product.findByIdAndRemove(req.body.id)
    .then(x => {
        res.status(200).send({message: 'Deu certo'});
    })
    .catch(e => {
        res.status(400).send({
            message: 'Falha'
        });
    })
};